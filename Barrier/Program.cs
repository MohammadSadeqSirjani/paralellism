﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace BarrierTasks
{
    internal static class Program
    {
        private static readonly Barrier threadBarrier = new Barrier(2, barrier =>
       {
           Console.WriteLine($"Phase {barrier.CurrentPhaseNumber} is finished.");
       });

        private static void Water()
        {
            Console.WriteLine("Putting the kettle on (takes a bit longer)");
            Thread.Sleep(2000);
            threadBarrier.SignalAndWait();
            Console.WriteLine("Pouring water into cup.");
            threadBarrier.SignalAndWait();
            Console.WriteLine("Putting the kettle away.");
        }

        private static void Cup()
        {
            Console.WriteLine("Finding the nicest cup of tea (fast)");
            threadBarrier.SignalAndWait();
            Console.WriteLine("Adding tea.");
            threadBarrier.SignalAndWait();
            Console.WriteLine("Adding sugar.");
        }

        private static void Main(string[] args)
        {
            var water = Task.Factory.StartNew(Water);
            var cup = Task.Factory.StartNew(Cup);

            var tea = Task.Factory.ContinueWhenAll(new[] { water, cup }, tasks =>
            {
                Console.WriteLine("Enjoy your cup of tea.");
            });

            tea.Wait();

            Console.WriteLine("Main process has been completed!");
        }
    }
}
