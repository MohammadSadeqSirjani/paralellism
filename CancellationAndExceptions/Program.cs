﻿using System;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading;
using System.Threading.Tasks;

namespace CancellationAndExceptions
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            ParallelLinq();
            Console.WriteLine("Main process has been completed!");
        }

        private static void ParallelLinq()
        {
            const int count = 20;

            var cts = new CancellationTokenSource();

            var items = ParallelEnumerable.Range(1, count);
            var cubes = items.WithCancellation(cts.Token)
                .Select(i => Math.Log10(i)).ToArray();

            try
            {
                foreach (var value in cubes)
                {
                    if (value > 1)
                        cts.Cancel();

                    Console.WriteLine($"[{value}]");
                }
            }
            catch (AggregateException error)
            {
                error.Handle(err =>
                {
                    Console.WriteLine($"{err.GetType().Name}: {err.Message}");
                    return true;
                });
            }
            catch (OperationCanceledException error)
            {
                Console.WriteLine($"{error.GetType().Name}: {error.Message}");
            }
        }
    }
}
