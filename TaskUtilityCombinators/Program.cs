﻿using System;
using System.Threading.Tasks;

namespace TaskUtilityCombinators
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await Task.WhenAll(MeasurePressure(), MeasureTemperature());
            await Task.WhenAny(DownloadFromFtp(), DownloadFromHttp());

            Console.WriteLine("Main process has been completed!");
        }

        private static async Task DownloadFromHttp()
        {
            await Task.Delay(1000);
            Console.WriteLine("Download from http");
            await Task.CompletedTask;
        }

        private static async Task DownloadFromFtp()
        {
            await Task.Delay(1000);
            Console.WriteLine("Download from ftp");
            await Task.CompletedTask;
        }

        private static async Task MeasureTemperature()
        {
            await Task.Delay(1000);
            Console.WriteLine("Measure temperature");
            await Task.CompletedTask;
        }

        private static async Task MeasurePressure()
        {
            await Task.Delay(1000);
            Console.WriteLine("Measure pressure");
            await Task.CompletedTask;
        }
    }
}
