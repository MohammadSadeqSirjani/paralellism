﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomAggregation
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            WithSum();
            WithAggregate();
            WithParallelAggregate();
            Console.WriteLine("Main process has been completed!");
        }

        private static void WithSum()
        {
            var sum = Enumerable.Range(1, 1000).Sum();
            Console.WriteLine($"sum(WithSum) = {sum}");
        }

        private static void WithAggregate()
        {
            var sum = Enumerable.Range(1, 1000).Aggregate(0, (i, acc) => i + acc);
            Console.WriteLine($"sum(WithAggregate) = {sum}");
        }

        private static void WithParallelAggregate()
        {
            var sum = ParallelEnumerable.Range(1, 1000).Aggregate(0,
            (partialSum, i) => partialSum += i,
            (total, subtotal) => total += subtotal,
            i => i);
            Console.WriteLine($"sum(WithParallelAggregate) = {sum}");
        }   
    }
}
