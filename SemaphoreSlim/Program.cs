﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreSlimContinues
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            SemaphoreContinues();

            Console.WriteLine("Main process has been completed!");
        }

        private static void SemaphoreContinues()
        {
            var semaphore = new SemaphoreSlim(2, 10);
            Console.WriteLine($"Semaphore count: {semaphore.CurrentCount}.");

            for (var i = 0; i < 20; i++)
            {
                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine($"Entering task {Task.CurrentId}.");
                    semaphore.Wait(); // releaseCount--
                    Console.WriteLine($"Processing task {Task.CurrentId}.");
                });
            }


            while (semaphore.CurrentCount <= 2)
            {
                Console.ReadKey();
                semaphore.Release(2); //releaseCount +- 2
            }
        }
    }
}
