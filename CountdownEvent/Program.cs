﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CountdownEventContinues
{
    internal static class Program
    {
        private static readonly int taskCount = 5;
        private static readonly CountdownEvent cte = new CountdownEvent(taskCount);
        private static readonly Random random = new Random();

        private static void Main(string[] args)
        {
            CountdownEventContinues();

            Console.WriteLine("Main process has been completed!");
        }

        private static void CountdownEventContinues()
        {
            //Barrier SignalAndWait
            //CountdownEvent Signal Wait

            for (var i = 0; i < taskCount; i++)
            {
                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine($"Entering task {Task.CurrentId}.");
                    Thread.Sleep(random.Next(3000));
                    cte.Signal();
                    Console.WriteLine($"Exiting task {Task.CurrentId}.");
                });
            }

            var finalTask = Task.Factory.StartNew(() =>
            {
                Console.WriteLine($"Waiting for other tasks to complete in {Task.CurrentId}.");
                cte.Wait();
                Console.WriteLine("All tasks has been completed.");
            });

            finalTask.Wait();
        }
    }
}
