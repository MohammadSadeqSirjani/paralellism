﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace AsParallelDemo
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            ParallelLinq();
            Console.WriteLine("Main process has been completed!");
        }

        private static void ParallelLinq()
        {
            const int count = 100;

            var items = Enumerable.Range(1, count).ToArray();
            var results = new int[count];

            items.AsParallel().ForAll(item =>
            {
                var value = item * item * item;
                Console.WriteLine($"[Line: {item}] [Cube: {value}] (Thread: {Task.CurrentId})");
                results[item - 1] = value;
            });

            var cubes = items.AsParallel().AsOrdered().Select(i => i * i * i);
        }
    }
}
