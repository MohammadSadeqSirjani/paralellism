﻿using System;
using System.Threading.Tasks;

namespace TaskRun
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var innerTask = Task.Factory.StartNew(() =>
            {
                var inner = Task.Factory.StartNew(() => { });
                return inner;
            });

            var asyncTask = Task.Factory.StartNew(async () =>
            {
                await Task.Delay(5000);
                return 123;
            });

            var unwrapAsyncTask = Task.Factory.StartNew(async () =>
            {
                await Task.Delay(5000);
                return 123;
            }).Unwrap();

            var runTask = Task.Run(async delegate
            {
                await Task.Delay(5000);
                return 132;
            });

            Console.WriteLine("Main process has been completed!");
        }
    }
}
