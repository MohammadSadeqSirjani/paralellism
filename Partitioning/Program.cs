﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace Partitioning
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var summery = BenchmarkRunner.Run<Program>();
            Console.WriteLine(summery);
            Console.WriteLine("Main process has been completed!");
        }

        [Benchmark]
        public void SquareEachValue()
        {
            const int count = 100000;
            var values = Enumerable.Range(0, count);
            var results = new int[count];
            Parallel.ForEach(values, value => { results[value] = (int)Math.Pow(value, 2); });
        }

        [Benchmark]
        public void SquareEachValueInChunkMode()
        {
            const int count = 100000;
            var values = Enumerable.Range(0, count);
            var results = new int[count];
            var part = Partitioner.Create(0, count, 10000);
            Parallel.ForEach(part, range =>
            {
                var (start, stop) = range;

                for (var i = start; i < stop; i++)
                {
                    results[i] = (int) Math.Pow(i, 2);
                }
            });
        }
    }
}
