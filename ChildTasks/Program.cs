﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChildTasks
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            ChildExecutionTasks();

            Console.WriteLine("Main process has been completed!");
        }

        private static void ChildExecutionTasks()
        {
            var parent = new Task(() =>
            {
                //detached
                var child = new Task(() =>
                {
                    Console.WriteLine("Child task starting ...");
                    Thread.Sleep(3000);
                    //throw  new AggregateException();
                    Console.WriteLine("Child task finishing ...");
                }, TaskCreationOptions.AttachedToParent);

                var completionHandler = child.ContinueWith(
                    task =>
                    {
                        Console.WriteLine(
                            $"Task {task.Id}'s state is {task.Status}. This means, this task has been completed.");
                    }, TaskContinuationOptions.AttachedToParent | TaskContinuationOptions.OnlyOnRanToCompletion);


                var failureHandler = child.ContinueWith(task =>
                    {
                        Console.WriteLine(
                            $"Task {task.Id}'s state is {task.Status}. This means, this task has been failed.");
                    }, TaskContinuationOptions.AttachedToParent | TaskContinuationOptions.OnlyOnFaulted);

                child.Start();
            });

            parent.Start();

            try
            {
                parent.Wait();
            }
            catch (AggregateException ae)
            {
                ae.Handle(err =>
                {
                    Console.WriteLine("Exception has been caught.");
                    return true;
                });
            }
        }
    }
}
