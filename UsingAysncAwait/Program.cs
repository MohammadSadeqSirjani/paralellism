﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UsingAsyncAwait
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //RunSync();
            //RunAsync();
            //RunAsyncAwait();
            Console.WriteLine("Main process has been completed!");
        }

        private static void RunSync()
        {
            var value = CalculateValue();
            Console.WriteLine($"Result = {value}");
        }

        private static void RunAsync()
        {
            var value = CalculateValueAsync();
            var result = "";
            value.ContinueWith(task => { result = task.Result.ToString(); },
                TaskScheduler.FromCurrentSynchronizationContext());

            Console.WriteLine($"Result = {result}");
        }

        private static async Task RunAsyncAwait()
        {
            var value = await CalculateValueAsyncAwait();
            Console.WriteLine($"Result = {value}");
        }

        private static int CalculateValue()
        {
            Thread.Sleep(5000);
            Console.WriteLine("Now, Main thread has been occupied with my process.\nMain UI has been crashed!");
            return 123;
        }

        private static Task<int> CalculateValueAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                Thread.Sleep(5000);
                Console.WriteLine("Now, My Process has is running on separate thread and main thead is free fot UI.");
                return 123;
            });
        }

        private static async Task<int> CalculateValueAsyncAwait()
        {
            await Task.Delay(5000);
            Console.WriteLine("Processing this task is asynchronous.");
            return 123;
        }
    }
}