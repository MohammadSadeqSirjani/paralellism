﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace BreakingCancellationsAndExceptions
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Demo();
            }
            catch (AggregateException error)
            {
                error.Handle(err =>
                {
                    Console.WriteLine(err);
                    return true;
                });
            }
            catch (OperationCanceledException)
            {

            }

            Console.WriteLine("Main process has been completed!");
        }

        private static void Demo()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;
            var option = new ParallelOptions()
            {
                CancellationToken = token
            };

            var result = Parallel.For(0, 20, option, (int i, ParallelLoopState state) =>
            {
                Console.WriteLine($"{i}[{Task.CurrentId}]\t");
                Thread.Sleep(100);
                token.ThrowIfCancellationRequested(); 
                if (i == 10)
                {
                    cts.Cancel();
                    //state.Stop();
                    //state.Break();
                    //throw new Exception();
                }
            });

            Console.WriteLine();
            Console.WriteLine($"Was the loop completed? {result.IsCompleted}.");
            Console.WriteLine(value: result.LowestBreakIteration.HasValue ? $"Lowest break iteration is {result.LowestBreakIteration}."
                : $"Lowest break iteration is {result.LowestBreakIteration.HasValue}.");
        }
    }
}
