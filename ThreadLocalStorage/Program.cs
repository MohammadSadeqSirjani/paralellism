﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLocalStorage
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            //InBeneficentSum();
            //BeneficentSum();
            Copy();

            Console.WriteLine("Main process has been completed!");
        }

        private static void BeneficentSum()
        {
            var sum = 0;
            Parallel.For(1, 1001, () => 0, (x, state, tls) =>
                {
                    tls += x;
                    Console.WriteLine($"Task {Task.CurrentId} has sum {tls}");
                    return tls;
                },
                partialSum =>
                {
                    Console.WriteLine($"Partial value of task {Task.CurrentId} is {partialSum}.");
                    Interlocked.Add(ref sum, partialSum);
                });

            Console.WriteLine($"Sum of 1..1000 = {sum}");
        }

        private static void Copy()
        {
            var sum = 0;
            Parallel.For(1, 1001, () => 0, (i, state, tls) =>
            {
                tls += i;
                return tls;
            }, partialSum => { Interlocked.Add(ref sum, partialSum); });

            Console.WriteLine($"Sum of 1..1000 = {sum}.");
        }

        private static void InBeneficentSum()
        {
            var sum = 0;
            Parallel.For(1, 1001, x =>
            {
                Console.WriteLine($"[{x}] ({Task.CurrentId})");
                Interlocked.Add(ref sum, x);
            });

            Console.WriteLine($"Sum of 1..1000 = {sum}");
        }
    }
}
