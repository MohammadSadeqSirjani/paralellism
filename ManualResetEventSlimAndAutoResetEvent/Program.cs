﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ManualResetEventSlimAndAutoResetEvent
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            //MakeTeaWithManualResetEventSlim();

            MakeTeaWithAutoRecent();

            Console.WriteLine("Main process has been completed!");
        }

        private static void MakeTeaWithAutoRecent()
        {
            var auto = new AutoResetEvent(false);

            Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Boiling water ...");
                Thread.Sleep(3000);
                auto.Set(); //true
            });

            var makeTea = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Waiting for water ...");
                auto.WaitOne(); //false false
                Console.WriteLine("Here is your tea.");
                var ok = auto.WaitOne(1000); // false
                Console.WriteLine(ok ? "Enjoy your tea." : "No tea for you.");
            });

            makeTea.Wait();
        }

        private static void MakeTeaWithManualResetEventSlim()
        {
            var recent = new ManualResetEventSlim();

            Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Boiling water ...");
                Thread.Sleep(3000);
                recent.Set();
            });

            var makeTea = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Waiting for water ...");
                recent.Wait();
                Console.WriteLine("Here is your tea.");
            });

            makeTea.Wait();
        }
    }
}
