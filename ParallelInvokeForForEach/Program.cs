﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParallelInvokeForForEach
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            //Invocation();

            //ParallelFor();

            //ParallelForEach();

            CostumeParallelFor();

            Console.WriteLine("Main process has been completed!");
        }

        private static void CostumeParallelFor()
        {
            var options = new ParallelOptions();
            Parallel.ForEach(Range(1, 20, 3), options, Console.WriteLine);
        }

        private static IEnumerable<int> Range(int start, int stop, int step)
        {
            for (var i = start; i < stop; i += step)
            {
                yield return i;
            }
        }

        private static void ParallelForEach()
        {
            var words = new[] { "oh", "what", "a", "night" };

            Parallel.ForEach(words, word =>
            {
                var length = word.Length;
                Console.WriteLine($"The '{word}' has " + (length > 1 ? $"{length} letters." : $"{length} letter.")
                                                       + $" (task {Task.CurrentId})");
            });
        }

        private static void ParallelFor()
        {
            Parallel.For(1, 11, i =>
            {
                Console.WriteLine($"This process has " +
                                  $"been completed on {Task.CurrentId} " +
                                  $"thread and release {i} value.");
            });
        }

        private static void Invocation()
        {
            var a = new Action(() => { Console.WriteLine($"First {Task.CurrentId}."); });
            var b = new Action(() => { Console.WriteLine($"Second {Task.CurrentId}."); });
            var c = new Action(() => { Console.WriteLine($"Third {Task.CurrentId}."); });

            Parallel.Invoke(a, b, c);
        }
    }
}
