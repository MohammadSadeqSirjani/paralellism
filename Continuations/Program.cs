﻿using System;
using System.Threading.Tasks;

namespace Continuations
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            ContinueWith();

            RunContinueWhenAll();

            RunContinueWhenAny();

            Console.WriteLine("Main process has been completed!");
        }

        private static void RunContinueWhenAll()
        {
            var task1 = Task.Factory.StartNew(() => "Task 1");
            var task2 = Task.Factory.StartNew(() => "Task 2");

            var tasks = Task.Factory.ContinueWhenAll(new[] { task1, task2 }, task =>
              {
                  Console.WriteLine("Task has been completed.\nThe tasks are:");
                  foreach (var item in task)
                  {
                      Console.WriteLine($" - {item.Result}");
                  }

                  Console.WriteLine("All tasks has been completed.");
              });

            tasks.Wait();
        }

        private static void RunContinueWhenAny()
        {
            var task1 = Task.Factory.StartNew(() => "Task 1");
            var task2 = Task.Factory.StartNew(() => "Task 2");

            var tasks = Task.Factory.ContinueWhenAny(new[] { task1, task2 }, task =>
            {
                Console.WriteLine("Task has been completed.\nThe tasks are:");
                Console.WriteLine($" - {task.Result}");
                Console.WriteLine("All tasks has been completed.");
            });

            tasks.Wait();
        }

        private static void ContinueWith()
        {
            var boilingWater = Task.Factory.StartNew(() => { Console.WriteLine("Boiling water ..."); });

            var waitingForBoiling = boilingWater.ContinueWith(task =>
            {
                Console.WriteLine($"Completed task {task.Id}, pour water into the cup.");
            });

            waitingForBoiling.Wait();
        }
    }
}
